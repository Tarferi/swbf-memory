package net.ithief.swbf.memory;

import net.ithief.std.memwatch.CoreMemory;
import net.ithief.std.memwatch.ExtendedKernelAdapter;
import net.ithief.std.memwatch.FailHandler;
import net.ithief.std.memwatch.RoutineExecutor;
import net.ithief.std.memwatch.exceptions.AttachmentFailedException;
import net.ithief.std.memwatch.exceptions.NoSuchProcessException;
import net.ithief.swbf.memory.exeptions.BattlefrontNotRunningException;

public class BattleFront {

	private void patchChat() throws NoSuchProcessException {
		ext.writeByteArray(addrChatEcho, bytear1);
		ext.writeByteArray(addrChatEchoRec, bytear2);
	}

	private static final byte[] bytear1 = new byte[] { (byte) 0xEB, 0x0A, (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90 };
	private static final byte[] bytear2 = new byte[] { (byte) 0x89, (byte) 0x86, (byte) 0x90, 0x04, 0x00, 0x00, (byte) 0x90, (byte) 0xEB, (byte) 0xF1 };

	private static final long addrChatSend = 0x005F597B;
	private static final long addrChatSendVal = 0x007FC410;
	protected static final long addrChatStr = 0x007FC412;
	private ExtendedKernelAdapter ext;

	private static final long addrChatEcho = 0x005A001BL;
	private static final long addrChatEchoRec = 0x005A0027L;
	private static final long addrChatEchoRedir = 0x005A002DL;
	private static final long addrChatEchoPointer = 0x0022F434L;
	private static final long addrChatTeam = 0x0022FA90L;

	private static final long addrCaptureText = 0x0022F884L;
	private static final long addrCaptureEcho = 0x0022F230L;
	private static final long addrCaptureColor = 0x0022F742L;

	public enum StatsMessageType {
		ALLY, ENEMY, NEUTRAL, UNKNOWN;
	}

	public BattleFront() throws BattlefrontNotRunningException {
		try {
			this.ext = new ExtendedKernelAdapter("battlefront.exe");
			patchChat();
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	public void setMessageReceiveListener(final MessageListener l) {
		ext.attach(new FailHandler() {

			@Override
			public void fail(AttachmentFailedException e) {
				e.printStackTrace();
			}

		});
		try {
			ext.addBreakpoint(addrChatEchoRedir, new RoutineExecutor() {

				@Override
				public void execute(long address, CoreMemory memory, int ThreadID) {
					try {
						int chataddr = memory.readInt(addrChatEchoPointer);
						if (chataddr == 2292088) {
							String str = memory.readString(chataddr, true);
							boolean team = memory.readByte(addrChatTeam) == 1;
							l.messageSend(team, str);
						} else {
							chataddr = memory.readInt(addrCaptureEcho);
							if (chataddr == addrCaptureText) {
								String stats = memory.readString(addrCaptureText, true);
								int col = memory.readByte(addrCaptureColor) & 0xff;
								if (col == 32) {
									l.statusMessage(StatsMessageType.ALLY, stats);
								} else if (col == 223) {
									l.statusMessage(StatsMessageType.ENEMY, stats);
								} else if (col == 192) {
									l.statusMessage(StatsMessageType.NEUTRAL, stats);
								} else {
									l.statusMessage(StatsMessageType.UNKNOWN, stats);
								}
							}
						}
					} catch (NoSuchProcessException e) {
						e.printStackTrace();
					}
				}

			});
		} catch (AttachmentFailedException | NoSuchProcessException e) {
			e.printStackTrace();
		}
	}

	public void setMessageSendListener(final MessageListener l) {
		ext.attach(null);
		try {
			ext.addBreakpoint(addrChatSend, new RoutineExecutor() {

				@Override
				public void execute(long address, CoreMemory memory, int ThreadID) {
					try {
						String str = memory.readString(addrChatStr, true);
						boolean team = memory.readByte(addrChatSendVal) == 2;
						l.messageSend(team, str);
					} catch (NoSuchProcessException e) {
						e.printStackTrace();
					}
				}

			});
		} catch (AttachmentFailedException | NoSuchProcessException e) {
			e.printStackTrace();
		}
	}

	public Battle getCurrentBattle() {
		return new Battle(ext);
	}

	public boolean battleInProgress() throws BattlefrontNotRunningException {
		Battle b = getCurrentBattle();
		return b.getBadSideReinforcements() != 0 || b.getGoodSideReinforcements() != 0;
	}

	public void detach() {
		ext.detach();
	}
}
