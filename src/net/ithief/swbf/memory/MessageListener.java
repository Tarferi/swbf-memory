package net.ithief.swbf.memory;

import net.ithief.swbf.memory.BattleFront.StatsMessageType;

public interface MessageListener {

	public void messageSend(boolean team, String message);
	
	public void statusMessage(StatsMessageType type, String message);
}
