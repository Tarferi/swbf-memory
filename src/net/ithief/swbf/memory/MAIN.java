package net.ithief.swbf.memory;

import java.util.List;

import net.ithief.swbf.memory.BattleFront.StatsMessageType;
import net.ithief.swbf.memory.exeptions.BattlefrontNotRunningException;

public class MAIN {

	public static void main(String[] args) {
		test3();
	}
	
	public static void test3() {
		try {
			BattleFront bw = new BattleFront();
			bw.setMessageReceiveListener(new MessageListener() {

				@Override
				public void messageSend(boolean team, String message) {
					System.out.println("Received " + (team ? "team " : "message: '") + message + "'");
				}

				@Override
				public void statusMessage(StatsMessageType type, String message) {
				}


			});
		} catch (BattlefrontNotRunningException e) {
			e.printStackTrace();
		}
	}

	public static void test2() {
		try {
			BattleFront bw = new BattleFront();
			bw.setMessageSendListener(new MessageListener() {

				@Override
				public void messageSend(boolean team, String message) {
					System.out.println("Sending " + (team ? "team " : "message: '") + message + "'");
				}

				@Override
				public void statusMessage(StatsMessageType type, String message) {
				}


			});
		} catch (BattlefrontNotRunningException e) {
			e.printStackTrace();
		}
	}

	public static void test1() {
		try {
			BattleFront bw = new BattleFront();
			Battle battle = bw.getCurrentBattle();
			System.out.println("Rein: " + battle.getGoodSideReinforcements() + ":" + battle.getBadSideReinforcements());
			System.out.println("Playing: " + battle.getCurrentMap());
			List<Player> players = battle.getPlayerList();
			for (Player p : players) {
				System.out.println("Player: " + p.getPlayerName() + " Kills: " + p.getPlayerKills() + " Deaths: " + p.getPlayerDeaths());
			}
		} catch (BattlefrontNotRunningException e) {
			e.printStackTrace();
		}
	}
}
