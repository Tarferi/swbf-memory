package net.ithief.swbf.memory;

import net.ithief.std.memwatch.CoreMemory;
import net.ithief.std.memwatch.exceptions.NoSuchProcessException;
import net.ithief.swbf.memory.exeptions.BattlefrontNotRunningException;

public class Player {

	private static final long addrPlayerKills = 0x0075D658L;
	private static final long addrPlayerKillsOffset = 0x58L;

	private static final long addrPlayerName = 0x0076E674L;
	private static final long addrPlayerNameOffset = 0x22C;

	private CoreMemory mem;
	private String name;
	private int index;
	private int kills;
	private int deaths;

	public Player(CoreMemory mem, int index) throws BattlefrontNotRunningException {
		this.mem = mem;
		this.index = index;
		update();
	}

	public void update() throws BattlefrontNotRunningException {
		this.name = getName();
		this.kills = getKills();
		this.deaths = getDeaths();
	}

	private String getName() throws BattlefrontNotRunningException {
		try {
			return mem.readString(addrPlayerName + (index * addrPlayerNameOffset), false);
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	private int getKills() throws BattlefrontNotRunningException {
		try {
			return mem.readInt(addrPlayerKills + (index * addrPlayerKillsOffset));
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	private int getDeaths() throws BattlefrontNotRunningException {
		try {
			return mem.readInt(addrPlayerKills + (index * addrPlayerKillsOffset));
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	public String getPlayerName() {
		return name;
	}

	public int getPlayerKills() {
		return kills;
	}

	public int getPlayerDeaths() {
		return deaths;
	}
}
