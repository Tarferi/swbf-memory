package net.ithief.swbf.memory;

import java.util.ArrayList;
import java.util.List;

import net.ithief.std.memwatch.CoreMemory;
import net.ithief.std.memwatch.exceptions.NoSuchProcessException;
import net.ithief.swbf.memory.exeptions.BattlefrontNotRunningException;

public class Battle {

	private CoreMemory mem;
	private static final long addrGoodRein = 0x04387844L;
	private static final long addrBadRein = 0x04387840L;

	private static final long addrCurrentMap = 0x007C2F04L;

	private static final int playerCount = 32;

	protected Battle(CoreMemory mem) {
		this.mem = mem;
	}

	public List<Player> getPlayerList() throws BattlefrontNotRunningException {
		List<Player> lst = new ArrayList<>();
		for (int i = 0; i < playerCount; i++) {
			Player pl = new Player(mem, i);
			if (!pl.getPlayerName().isEmpty()) {
				lst.add(pl);
			}
		}
		return lst;
	}

	public String getCurrentMap() throws BattlefrontNotRunningException {
		try {
			return mem.readString(addrCurrentMap, false);
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	public int getGoodSideReinforcements() throws BattlefrontNotRunningException {
		try {
			return mem.readShort(addrGoodRein);
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}

	public int getBadSideReinforcements() throws BattlefrontNotRunningException {
		try {
			return mem.readShort(addrBadRein);
		} catch (NoSuchProcessException e) {
			throw new BattlefrontNotRunningException();
		}
	}
}
